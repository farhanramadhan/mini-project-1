const express = require('express')
const app = express()
const path = require('path')
const port = 8080

//app.get('/', (req, res) => res.send('Welcome'))


app.get('/', (req, res) => {
    res.sendFile('index.html', {
        root: path.join(__dirname, './')
    })
})

app.get('/index1.html', (req, res) => {
    res.sendFile('index1.html', {
        root: path.join(__dirname, './')
    })
})

app.listen(port, () => console.log(`App listening on port ${port}!`))