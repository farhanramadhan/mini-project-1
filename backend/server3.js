var express = require('express');
var express_graphql = require('express-graphql');
var { buildSchema } = require('graphql');
var axios = require('axios');
const cors = require('cors');

//GraphQL schema
var schema = buildSchema(`

    type Query{
        issues: Issue
    }

    type Author{
        login: String
    }

    type Nodesauthor{
        author: Author
        bodyText: String
    }

    type Comments{
        nodes: [Nodesauthor]
    }

    type Nodes {
        number: Int
        title: String
        author: Author
        comments: Comments
    }

    type Issue {
        nodes: [Nodes]
    }
`);

async function getGitHubIssue(){ 
    
    const res = await axios({
        url: 'https://api.github.com/graphql',
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'bearer 02565b8874d4426bcf60a917bb3c55980435a15f'
        },
        data: {
            query: `
        {
            repository(owner:"facebook", name:"react") {
                name
                issues(first:25){
                    nodes {
                    number
                    title
                        author{
                            login
                    }
                    comments(first:25){
                        nodes{
                            author{
                                login
                            }
                            bodyText
                        }
                    }
                    }
                }
            }
        }
            `
        }
    })
// console.log(res.data.data.repository.issues)
return res.data.data.repository.issues

}
//resolver
var root = {
    async issues() {
        return await getGitHubIssue()
    }
}

// Create an express server and a GraphQL endpoint
var app = express();

app.use(cors({
	origin: true,
	credentials: true,
}));

app.use('/graphql', express_graphql({
    schema: schema,
    rootValue: root,
    graphiql: true
}));
app.listen(8080, () => console.log('Express GraphQL Server Now Running On localhost:8080/graphql'));
